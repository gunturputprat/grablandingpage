import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import { Dimensions } from "react-native";
import IconFood from "../assets/images/icon/icon-food.png";
import IconMart from "../assets/images/icon/icon-mart.png";
import IconExpress from "../assets/images/icon/icon-express.png";
import IconPulsa from "../assets/images/icon/icon-pulsa.png";
import IconCar from "../assets/images/icon/icon-car.png";
import IconiBike from "../assets/images/icon/icon-bike.png";
import IconInsurance from "../assets/images/icon/icon-insurance.png";
import IconMore from "../assets/images/icon/icon-more.png";
import IconCalendar from "../assets/images/icon/icon-calendar.png";
import IconOvo from "../assets/images/icon/icon-ovo.png";
import IconTopup from "../assets/images/icon/icon-topup.png";
import IconPoints from "../assets/images/icon/icon-points.png";

import ArticleImages1 from "../assets/images/articles/article1.png";
import ArticleImages2 from "../assets/images/articles/article2.png";
import ArticleImages3 from "../assets/images/articles/article3.png";
import ArticleImages4 from "../assets/images/articles/article4.png";
import ArticleImages5 from "../assets/images/articles/article5.png";
import ArticleImages6 from "../assets/images/articles/article6.png";
import ArticleImages7 from "../assets/images/articles/article7.png";
import ArticleImages8 from "../assets/images/articles/article8.png";

const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

export default function Mainmenu() {
  const images = [
    { src: IconFood, title: "Food" },
    { src: IconMart, title: "Mart" },
    { src: IconExpress, title: "Express" },
    { src: IconPulsa, title: "Pulsa/Token" },
    { src: IconCar, title: "Car" },
    { src: IconiBike, title: "Bike" },
    { src: IconInsurance, title: "Insurance" },
    { src: IconMore, title: "More" },
  ];
  const articles = [
    {
      src: ArticleImages1,
      title: "NgeGrabFood pakai kartu kredit/debit aja",
      date: "22 Nov",
      category: "new",
    },
    {
      src: ArticleImages2,
      title: "Kejar diskon s.d Rp40rb & menangkan EMAS",
      date: "22 Nov",
      category: "new",
    },
    {
      src: ArticleImages3,
      title: "Gencar makan, bisa menang total 1 miliar",
      date: "13 Dec",
      category: "old",
    },
    {
      src: ArticleImages4,
      title: "Ambil diskon s.d 50% dari Top Resto Fest",
      date: "10 Dec",
      category: "old",
    },
    {
      src: ArticleImages5,
      title: "NgeGrabFood pakai kartu kredit/debit aja",
      date: "22 Nov",
      category: "old",
    },
    {
      src: ArticleImages6,
      title: "Kejar diskon s.d Rp40rb & menangkan EMAS",
      date: "22 Nov",
      category: "old",
    },
    {
      src: ArticleImages7,
      title: "Gencar makan, bisa menang total 1 miliar",
      date: "13 Dec",
      category: "old",
    },
    {
      src: ArticleImages8,
      title: "Ambil diskon s.d 50% dari Top Resto Fest",
      date: "10 Dec",
      category: "old",
    },
  ];
  return (
    <Grid>
      <View style={styles.container}>
        {/* submenu */}
        <Row style={styles.subMenu}>
          <Col style={styles.subMenuLeft}>
            <View>
              <Text style={styles.iconSubMenu}>
                <Image source={IconOvo} />
                {"  "}
                IDR0
              </Text>
            </View>
          </Col>
          <Col style={styles.subMenuRight}>
            <View>
              <Text style={styles.iconSubMenu}>
                <Image source={IconPoints} />
                {"  "}
                Hello, OVO Points
              </Text>
            </View>
          </Col>
        </Row>
        {/* wallet */}
        <Row style={styles.subMenuWallet}>
          <View style={styles.wallet}>
            <Text style={styles.iconSubMenu}>
              <Image source={IconTopup} />
              {"  "}
              Top Up Wallet
            </Text>
          </View>
        </Row>
        {/* products */}
        <Row>
          <View style={styles.containerList}>
            {images.map((item) => (
              <View style={styles.listMenu}>
                <Image style={styles.iconMenu} source={item.src}></Image>
                <Text style={styles.listMenuTitle}>{item.title}</Text>
              </View>
            ))}
          </View>
        </Row>
        {/* article */}
        <Row>
          <View style={styles.containerListArticle}>
            <View style={styles.category}>
              <Text style={styles.categoryTitle}>Anti Tanggal Tua</Text>
            </View>
            {articles.map((article) => {
              if (article.category === "new") {
                return (
                  <View style={styles.listMenuView}>
                    <Image
                      style={styles.listMenuArticle}
                      source={article.src}></Image>
                    <Text style={styles.listMenuTitleArticle}>
                      {article.title}
                    </Text>
                    <Text style={styles.listMenuAdditional}>
                      <Image
                        style={styles.iconCalendar}
                        source={IconCalendar}
                      />
                      Until {article.date}
                    </Text>
                  </View>
                );
              }
            })}
            <View style={styles.category}>
              <Text style={styles.categoryTitle}>Keep discovering</Text>
            </View>
            {articles.map((article) => {
              if (article.category === "old") {
                return (
                  <View style={styles.listMenuView}>
                    <Image
                      style={styles.listMenuArticle}
                      source={article.src}></Image>
                    <Text style={styles.listMenuTitleArticle}>
                      {article.title}
                    </Text>
                    <Text style={styles.listMenuAdditional}>
                      <Image
                        style={styles.iconCalendar}
                        source={IconCalendar}
                      />
                      Until {article.date}
                    </Text>
                  </View>
                );
              }
            })}
          </View>
        </Row>
      </View>
    </Grid>
  );
}

const styles = StyleSheet.create({
  container: {
    width: windowWidth,
    height: windowHeight,
  },
  subMenu: {
    backgroundColor: "#ffffff",
    height: 60,
  },
  subMenuWallet: {
    marginBottom: "auto",
    backgroundColor: "#ffffff",
    height: 60,
  },
  wallet: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ffffff",
    height: 40,
    marginTop: 10,
    width: 160,
    marginLeft: "auto",
    marginRight: "auto",
    marginBottom: "auto",
    borderWidth: 1,
    borderRadius: 6,
    borderColor: "#ddd",
    borderBottomWidth: 1,
    shadowColor: "#000000",
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowColor: "grey",
    elevation: 1,
    padding: 10,
  },
  iconSubMenu: {
    height: 60,
  },
  subMenuLeft: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 60,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderBottomColor: "#E1E1E1",
    borderRightColor: "#E1E1E1",
    textAlign: "center",
  },
  subMenuRight: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 60,
    borderBottomWidth: 1,
    borderBottomColor: "#E1E1E1",
    borderLeftColor: "#E1E1E1",
    textAlign: "center",
  },
  containerList: {
    backgroundColor: "#ffffff",
    flexDirection: "row",
    flexWrap: "wrap",
    height: 230,
    width: windowWidth,
    padding: 20,
    marginLeft: "auto",
    marginRight: "auto",
    justifyContent: "center",
    alignItems: "center",
  },
  containerListArticle: {
    borderTopWidth: 1,
    borderTopColor: "#E1E1E1",
    marginTop: -70,
    backgroundColor: "#ffffff",
    flexDirection: "row",
    width: windowWidth,
    height: 1050,
    flexWrap: "wrap",
  },
  listMenu: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: "auto",
    marginRight: "auto",
    padding: 10,
  },
  iconMenu: {
    height: 60,
    width: 60,
  },
  listMenuView: {
    width: windowWidth / 2,
    alignItems: "center",
    marginLeft: "auto",
    marginRight: "auto",
    padding: 10,
  },
  listMenuTitle: {
    fontWeight: "bold",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: "auto",
    marginRight: "auto",
  },
  listMenuTitleArticle: {
    fontWeight: "bold",
    paddingLeft: 7,
    flexWrap: "wrap",
    color: "#000000",
  },
  listMenuAdditional: {
    left: -40,
  },
  listMenuArticle: {
    height: 170,
    width: 170,
  },
  iconCalendar: {
    height: 15,
    width: 15,
    marginRight: 10,
  },
  category: {
    width: windowWidth,
    paddingLeft: 20,
    paddingTop: 20,
  },
  categoryTitle: {
    fontWeight: "bold",
    fontSize: 18,
    color: "#000000",
  },
});
