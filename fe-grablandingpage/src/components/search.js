import React from "react";
import { StyleSheet, View, Image } from "react-native";
import { SearchBar } from "react-native-elements";

export default function Search({ query }) {
  return (
    <View style={styles.header}>
      <View style={styles.containerImage}>
        <Image
          style={styles.ImageStyle}
          source={require("../assets/images/icon/icon-qr-code-scan.png")}
        />
      </View>

      <SearchBar
        value={query}
        containerStyle={styles.search}
        placeholder="Offers, food, and places to go"
        placeholderTextColor="#A6A6A6"
        inputStyle={{ backgroundColor: "#FFFFFF" }}
        inputContainerStyle={{
          height: 40,
          backgroundColor: "#FFFFFF",
        }}
        leftIconContainerStyle={{ backgroundColor: "#FFFFFF" }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  search: {
    flex: 1,
    backgroundColor: "#34C072",
    paddingLeft: 0,
    borderRadius: 0,
  },
  header: {
    backgroundColor: "#34C072",
    height: 60,
    flexDirection: "row",
    marginLeft: "auto",
    marginBottom: "auto",
  },
  containerImage: {
    height: 40,
    width: 45,
    marginLeft: 10,
    marginTop: 9,
    paddingLeft: 10,
    paddingTop: 10,
    right: 0,
    backgroundColor: "#F6F2F3",
  },
  ImageStyle: {
    height: 25,
    width: 25,
  },
});
