import React from "react";
import { StyleSheet, View, ScrollView } from "react-native";

import Search from "../components/Search";
import Mainmenu from "../components/Mainmenu";

export default class Home extends React.Component {
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Search></Search>
          <Mainmenu></Mainmenu>
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    height: 1400,
    justifyContent: "center",
    alignItems: "center",
  },
});
