import React from "react";
import { StyleSheet, View } from "react-native";

import Search from "../components/Search";
import Mainmenu from "../components/Mainmenu";

export default class Payment extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Search></Search>
        <Mainmenu></Mainmenu>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
