package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"

	"lemonilo/common/config"
	"lemonilo/common/database"
	"lemonilo/common/inject"
	"lemonilo/handler/rest"
)

const (
	port = 6969
)

func main() {
	cfg := config.InitConfig()
	dbConns := database.NewConnection(cfg)
	routes := rest.InitPackage()

	inject.DependencyInjection(cfg, dbConns, routes)

	router := httprouter.New()
	routes.InitRouter(router)

	log.Printf("Servis running on port: %v", cfg.Port.Rest)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", cfg.Port.Rest), router))
}
