package config

import (
	"log"

	"gopkg.in/gcfg.v1"
)

// InitConfig - readall .ini configuration file and applied to struct
func InitConfig() *Config {
	cfg := Config{}

	path := "files/lemonilo/lemonilo.ini"
	err := gcfg.ReadFileInto(&cfg, path)
	if err != nil {
		log.Fatalf("Failed read config from: %v, err: %v", path, err)
	}

	return &cfg
}
