package config

type (
	Config struct {
		Database Database
		Port     Port
	}

	Database struct {
		Username       string `gcfg:"username"`
		Password       string `gcfg:"password"`
		Host           string `gcfg:"host"`
		DbName         string `gcfg:"db-name"`
		Port           string `gcfg:"port"`
		Driver         string `gcfg:"driver"`
		MaxConnection  int    `gcfg:"max-connection"`
		IdleConnection int    `gcfg:"idle-connection"`
	}

	Port struct {
		Rest int `gcfg:"rest"`
	}
)
