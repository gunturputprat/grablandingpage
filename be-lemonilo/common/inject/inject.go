package inject

import (
	"log"

	"github.com/facebookgo/inject"
	"github.com/jmoiron/sqlx"

	"lemonilo/common/config"
	"lemonilo/handler/rest"
	handlerUser "lemonilo/handler/rest/user"
)

// DependencyInjection apply dependencies injection
func DependencyInjection(cfg *config.Config, dbConn *sqlx.DB, routes *rest.Routes) {
	dependencies := []*inject.Object{
		&inject.Object{Value: cfg, Name: "cfg"},
		&inject.Object{Value: dbConn, Name: "db-conn"},
		&inject.Object{Value: routes, Name: "routes"},
		&inject.Object{Value: &handlerUser.Handler{}, Name: "handler"},
	}

	var g inject.Graph
	if err := g.Provide(dependencies...); err != nil {
		log.Fatalf("Failed inject dependencies, err: %v", err)
	}

	if err := g.Populate(); err != nil {
		log.Fatalf("Failed inject populate dependencies, err: %v", err)
	}
}
