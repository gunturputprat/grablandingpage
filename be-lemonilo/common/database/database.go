package database

import (
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"

	"lemonilo/common/config"
)

// NewConnection for init connection to DB
func NewConnection(cfg *config.Config) *sqlx.DB {
	connString := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v", cfg.Database.Username, cfg.Database.Password, cfg.Database.Host, cfg.Database.Port, cfg.Database.DbName)
	dbConn, err := sqlx.Connect(cfg.Database.Driver, connString)
	if err != nil {
		log.Fatal("Failed to connect -> ", cfg.Database.DbName, ", err ->", err)
	}
	dbConn.SetMaxIdleConns(cfg.Database.IdleConnection)
	dbConn.SetMaxOpenConns(cfg.Database.MaxConnection)

	return dbConn
}
