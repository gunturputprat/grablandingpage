package rest

import (
	handlerUser "lemonilo/handler/rest/user"
)

type (
	Routes struct {
		Handler handlerUser.IHandler `inject:"handler"`
	}
)
