package handler

import (
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/jmoiron/sqlx"
	"github.com/julienschmidt/httprouter"
)

type (
	IHandler interface {
		Register(http.ResponseWriter, *http.Request, httprouter.Params)
		GetUserByID(http.ResponseWriter, *http.Request, httprouter.Params)
		GetAllUser(http.ResponseWriter, *http.Request, httprouter.Params)
		UpdateUser(http.ResponseWriter, *http.Request, httprouter.Params)
		DeleteUserByID(http.ResponseWriter, *http.Request, httprouter.Params)
		Login(http.ResponseWriter, *http.Request, httprouter.Params)
	}

	Handler struct {
		DbConn *sqlx.DB `inject:"db-conn"`
	}

	UserData struct {
		UserID   int64  `json:"user_id" db:"user_id"`
		Email    string `json:"email" db:"email"`
		Password string `json:"password,omitempty" db:"password"`
		Address  string `json:"address" db:"address"`
	}

	Login struct {
		Email    string `json:"email" db:"email"`
		Password string `json:"password" db:"password"`
	}

	TokenInfo struct {
		UserID  int64  `json:"user_id"`
		Email   string `json:"email"`
		Address string `json:"address"`
		*jwt.StandardClaims
	}

	Resp struct {
		Header Header      `json:"header"`
		Data   interface{} `json:"data"`
	}

	Header struct {
		Messages string `json:"messages"`
		Reason   string `json:"reason"`
	}
)

const (
	inserQuery = `
		INSERT INTO
			user(
				email,
				password,
				address
			)
		VALUES
			(?, ?, ?);
	`

	getUserByID = `SELECT user_id, email, address FROM user WHERE user_id = ?`

	getAllUser = `SELECT user_id, email, address FROM user`

	updateUser = `
		UPDATE
			user
		SET
			email = ?,
			address = ?
		WHERE
			user_id = ?
	`

	deleteUser = `DELETE FROM user where user_id = ?`

	login = "SELECT user_id, email, address FROM user WHERE email = ? AND password = ?"
)
