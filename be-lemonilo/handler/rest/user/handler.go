package handler

import (
	"crypto/md5"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/julienschmidt/httprouter"
)

func (h *Handler) Register(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	reqBody := UserData{}

	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		writeErrorResponse(w, http.StatusBadRequest, "Failed to encode request", err.Error())
		return
	}

	reqBody.Password = generatePassword(reqBody.Password)

	_, err = h.DbConn.Exec(
		inserQuery,
		reqBody.Email,
		reqBody.Password,
		reqBody.Address,
	)
	if err != nil {
		writeErrorResponse(w, http.StatusInternalServerError, "Failed insert new user", err.Error())
		return
	}

	writeSuccessResponse(w, nil)
	return
}

func (h *Handler) GetUserByID(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	userIDStr := p.ByName("user_id")
	userID, err := strconv.ParseInt(userIDStr, 10, 64)
	if userID <= 0 || err != nil {
		msg := ""
		if err != nil {
			msg = err.Error()
		}
		writeErrorResponse(w, http.StatusBadRequest, "Invalid user id", msg)
	}

	result := UserData{}
	err = h.DbConn.Get(&result, getUserByID, userID)
	if err != nil && err != sql.ErrNoRows {
		writeErrorResponse(w, http.StatusInternalServerError, "Failed when get user by id", err.Error())
		return
	}

	if err == sql.ErrNoRows {
		writeErrorResponse(w, http.StatusNotFound, "user id not found", err.Error())
		return
	}

	writeSuccessResponse(w, result)
	return
}

func (h *Handler) GetAllUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	result := []UserData{}
	err := h.DbConn.Select(&result, getAllUser)
	if err != nil {
		writeErrorResponse(w, http.StatusInternalServerError, "Failed when get data user", err.Error())
		return
	}

	writeSuccessResponse(w, result)
	return
}

func (h *Handler) UpdateUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	reqBody := UserData{}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		writeErrorResponse(w, http.StatusBadRequest, "Failed to decode request", err.Error())
		return
	}

	userIDStr := p.ByName("user_id")
	userID, err := strconv.ParseInt(userIDStr, 10, 64)
	if userID <= 0 || err != nil {
		msg := ""
		if err != nil {
			msg = err.Error()
		}
		writeErrorResponse(w, http.StatusBadRequest, "Invalid user id", msg)
	}

	_, err = h.DbConn.Exec(
		updateUser,
		reqBody.Email,
		reqBody.Address,
		userID,
	)
	if err != nil {
		writeErrorResponse(w, http.StatusInternalServerError, "Failed when update user data", err.Error())
		return
	}

	writeSuccessResponse(w, nil)
	return
}

func (h *Handler) DeleteUserByID(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	userIDStr := p.ByName("user_id")
	userID, err := strconv.ParseInt(userIDStr, 10, 64)
	if userID <= 0 || err != nil {
		msg := ""
		if err != nil {
			msg = err.Error()
		}
		writeErrorResponse(w, http.StatusBadRequest, "Invalid user id", msg)
	}

	_, err = h.DbConn.Exec(deleteUser, userID)
	if err != nil {
		writeErrorResponse(w, http.StatusInternalServerError, "Failed delete user data", err.Error())
		return
	}

	writeSuccessResponse(w, nil)
	return
}

func (h *Handler) Login(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	reqBody := Login{}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		writeErrorResponse(w, http.StatusBadRequest, "Failed to decode request", err.Error())
		return
	}

	reqBody.Password = generatePassword(reqBody.Password)
	result := UserData{}
	err = h.DbConn.Get(&result, login, reqBody.Email, reqBody.Password)
	if err != nil && err != sql.ErrNoRows {
		writeErrorResponse(w, http.StatusInternalServerError, "Failed when get user by email and password", err.Error())
		return
	}

	if err == sql.ErrNoRows {
		writeErrorResponse(w, http.StatusNotFound, "email or password not correct", err.Error())
		return
	}

	tokenString, err := setAuth(result)
	if err != nil {
		writeErrorResponse(w, http.StatusInternalServerError, "Failed set auth", err.Error())
		return
	}

	writeSuccessResponse(w, tokenString)
	return
}

// generatePassword generate hash password
func generatePassword(password string) string {
	md5 := md5.New()
	md5.Write([]byte(password))
	return fmt.Sprintf("%x", md5.Sum(nil))
}

// setAuth generate token login
func setAuth(userData UserData) (string, error) {
	tk := TokenInfo{
		UserID:  userData.UserID,
		Email:   userData.Email,
		Address: userData.Address,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 96).Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, err := token.SignedString([]byte("sUp3rs3cr3t"))
	if err != nil {
		return tokenString, err
	}

	return tokenString, err
}

func writeSuccessResponse(w http.ResponseWriter, data interface{}) {
	writeResponse(w, data, http.StatusAccepted, "", "")
}

func writeErrorResponse(w http.ResponseWriter, code int, messages string, reason string) {
	writeResponse(w, nil, code, messages, reason)
}

func writeResponse(w http.ResponseWriter, data interface{}, code int, messages string, reason string) {
	res := Resp{}
	res.Header.Messages = messages
	res.Header.Reason = reason
	res.Data = data
	msg, _ := json.Marshal(res)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(msg)
}
