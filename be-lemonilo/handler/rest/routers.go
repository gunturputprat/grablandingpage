package rest

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func InitPackage() *Routes {
	return &Routes{}
}

// InitRouter all router init in here
func (r *Routes) InitRouter(router *httprouter.Router) {

	router.POST("/register", r.Handler.Register)
	router.GET("/alluser", r.Handler.GetAllUser)
	router.GET("/user/:user_id", r.Handler.GetUserByID)
	router.PATCH("/user/:user_id", r.Handler.UpdateUser)
	router.DELETE("/user/:user_id", r.Handler.DeleteUserByID)
	router.POST("/login", r.Handler.Login)

	router.GET("/ping", func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		w.Write([]byte("Pong"))
		return
	})

}
