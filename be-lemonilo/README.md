## HOW TO RUN

1. Install mySql on the laptop (doc: https://www.javatpoint.com/how-to-install-phpmyadmin-on-mac)

2. Install Go lang environtment (doc: https://golang.org/doc/install)

3. First, create the database on mySql. eg : lemonilo

4. Second, import the table to database by copy the command inside files/sql/table.sql

5. Custom the environment inside files/lemonilo/lemonilo.ini based on your mySql

6. Run the golang with command 'make' in the terminal (without quote)

7. Test the API on Postmant by import the collection

```
source :
```

https://www.getpostman.com/collections/c9c9ff99575916fdbeed

```

```

Thank you

Cheers
Guntur Putra Pratama
