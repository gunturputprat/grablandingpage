CREATE TABLE user (
    user_id int AUTO_INCREMENT PRIMARY KEY,
    email varchar(32) not null unique,
    password varchar(32) not null,
    address varchar(200) not null
);
create index user_email_password on user (email, password);